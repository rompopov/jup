package com.roman.up.props;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyHandler {

    private final Properties properties;
    private DBProps dbProps;

    public PropertyHandler() {
        properties = new Properties();
        try (InputStream in = getClass().getClassLoader().getResourceAsStream("db.properties")) {
            if (in == null) {
                throw new FileNotFoundException();
            }
            properties.load(in);
        } catch (IOException e) {
            throw new IllegalArgumentException("db props is failed");
        }
    }

    public String loadProps(String key) {
        return properties.getProperty(key);
    }

    public DBProps getDBProps() {
        if (dbProps == null) {
            dbProps = new DBProps();
            dbProps.setUrl(loadProps("url"));
            dbProps.setUser(loadProps("user"));
            dbProps.setPass(loadProps("pass"));
            dbProps.setDriver(loadProps("driver"));
        }
        return dbProps;
    }

    public static class DBProps {
        private String url;
        private String user;
        private String pass;
        private String driver;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            if (url == null || url.isBlank()) {
                throw new IllegalArgumentException("Url can't be empty");
            }
            this.url = url;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            if (user == null || user.isBlank()) {
                throw new IllegalArgumentException("User can't be empty");
            }
            this.user = user;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            if (pass == null || pass.isBlank()) {
                throw new IllegalArgumentException("Password can't be empty");
            }
            this.pass = pass;
        }

        public String getDriver() {
            return driver;
        }

        public void setDriver(String driver) {
            if (pass == null || pass.isBlank()) {
                throw new IllegalArgumentException("Driver can't be empty");
            }
            this.driver = driver;
        }
    }
}