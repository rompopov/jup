package com.roman.up;

import com.roman.up.connect.ConnectionStarter;
import com.roman.up.connect.impl.PostgreConnectionStarter;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Starter {


    private static final String CREATE_TABLE = "create table if not exists BOOKS (\n" +
            "    id int primary key,\n" +
            "    name varchar(50) not null,\n" +
            "    author varchar(100) \n" +
            ")";

    public static void main(String[] args) {
        ConnectionStarter starter = PostgreConnectionStarter.getInstance();
        final Connection connection = starter.startConnect();

        try (Statement statement = connection.createStatement()) {
            statement.execute(CREATE_TABLE);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
