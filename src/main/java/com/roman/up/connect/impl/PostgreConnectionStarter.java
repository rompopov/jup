package com.roman.up.connect.impl;

import com.roman.up.connect.ConnectionStarter;
import com.roman.up.props.PropertyHandler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgreConnectionStarter implements ConnectionStarter {

    private Connection connection;
    private static PostgreConnectionStarter starter;
    private final PropertyHandler propertyHandler;

    private PostgreConnectionStarter() {
        propertyHandler = new PropertyHandler();
        new org.postgresql.Driver();
    }

    public static PostgreConnectionStarter getInstance() {
        if (starter == null) {
            return new PostgreConnectionStarter();
        }
        return starter;
    }

    @Override
    public Connection startConnect() {
        try {
            if (connection == null || connection.isClosed()) {
                final PropertyHandler.DBProps dbProps = propertyHandler.getDBProps();
                connection = DriverManager.getConnection(dbProps.getUrl(), dbProps.getUser(), dbProps.getPass());
            }
        } catch (SQLException e) {
            throw new IllegalStateException("Can't create connection", e);
        }
        return connection;
    }
}
