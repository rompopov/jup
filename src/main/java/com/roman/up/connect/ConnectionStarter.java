package com.roman.up.connect;

import java.sql.Connection;

public interface ConnectionStarter {

    Connection startConnect();
}